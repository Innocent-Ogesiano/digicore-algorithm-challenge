package com.company;

class RecursiveMain {
  public static void main(String[] args) {
    RecursiveMain recursion = new RecursiveMain();
    int rec = factorial(-2);
    System.out.println(rec);
  }

  public static int factorial(int n) {
    if (n<1) {
      return -1;
    }
    if (n==1) {
      return 1;
    }
    return n * factorial(n-1);
  }


}
