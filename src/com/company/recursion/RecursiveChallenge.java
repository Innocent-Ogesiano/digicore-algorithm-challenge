package com.company.recursion;

import java.util.Arrays;
import java.util.Collections;

public class RecursiveChallenge {
    public static void main(String[] args) {
        RecursiveChallenge challenge = new RecursiveChallenge();
        System.out.println(challenge.reverse("java"));
        System.out.println(challenge.isPalindrome("cocoa"));
        System.out.println(first("appMillers"));
        System.out.println(capitalizeWord("i love you"));
        int[] array = {1,2,3,4,5};
        reverseArray(array);
        int[] arr = Arrays.copyOf(array, array.length);
        System.out.println("Second Array: \n"+Arrays.toString(arr));
        System.out.println(Arrays.toString(middle(array)));
        int[][] myArray2D = {{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,16}};
        System.out.println(challenge.sumDiagonal(myArray2D));
        Integer[] myArray = {84,90,86,87,85,90,85,83,23,45,84,1,2,0};
        System.out.println(firstSecond(myArray));
        System.out.println(missingNumber(new int[]{1,2,3,4,6}, 6));
    }

    public String reverse(String str)
    {
//        if (str.isEmpty())
//            return str;
//        //Calling Function Recursively
//        return reverse(str.substring(1)) + str.charAt(0);

        StringBuilder s = new StringBuilder();
        for (int i = str.length() - 1; i >= 0 ; i--) {
            s.append(str.charAt(i));
        }
        return s.toString().trim();
    }

    public  boolean isPalindrome(String s)
    {   // if length is 0 or 1 then String is palindrome
//        if(s.length() == 0 || s.length() == 1)
//            return true;
//        if(s.charAt(0) == s.charAt(s.length()-1))
//            return isPalindrome(s.substring(1, s.length()-1));
//        return false;
        StringBuilder str = new StringBuilder();
        for (int i = s.length() - 1; i >= 0 ; i--) {
            str.append(s.charAt(i));
        }
        return str.toString().trim().equalsIgnoreCase(s);
    }

    static char first(String str) {
        if(str.isEmpty()) {
            return ' ';
        }
        if (Character.isUpperCase(str.charAt(0))) {
            return str.charAt(0);
        }else {
            return first(str.substring(1));
        }
//        for (int i = 0; i < str.length(); i++) {
////            if (str.charAt(i) &gt; 64 &amp;&amp; str.charAt(i) &lt; 91) {
////                return str.charAt(i);
////            }
//            if (Character.isUpperCase(str.charAt(i))) {
//                return str.charAt(i);
//            }
//        }
//        return ' ';
    }

    public static String capitalizeWord(String str){

        if(str.isEmpty()) {
            return str;
        }
        char chr = str.charAt(str.length()-1);
        if(str.length()==1) {
            return Character.toString(Character.toUpperCase(chr));
        }
        if((str.charAt(str.length() - 2) == ' ')) {
            chr = Character.toUpperCase(chr);
        }
        return capitalizeWord(str.substring(0,str.length()-1))+ chr;
//        String[] s = str.split("");
//        for (int i = 0; i < s.length; i++) {
//            if (i == 0) s[i] = s[i].toUpperCase();
//            else if (s[i].equals(" ")) {
//                s[i + 1] = s[i + 1].toUpperCase();
//            }
//        }
//        return String.join("", s).trim();
        //   TODO
    }

    static void reverseArray (int[] array) {
        for (int i = 0; i < array.length/2; i++) {
            int other = array.length - i - 1;
            int temp = array[i];
            array[i] = array[other];
            array[other] = temp;
        }
        System.out.println(Arrays.toString(array));
    }

    static int[] middle(int[] arr) {
        int[] midArr = new int[arr.length-2];
        System.arraycopy(arr, 1, midArr, 0, arr.length - 1 - 1);
        return midArr;
//        return Arrays.copyOfRange(arr, 1, arr.length-1);
    }

    public int sumDiagonal(int[][] a) {
        int sum = 0;
        for (int i=0; i<a.length;i++) {
            sum+= a[i][i];
        }
//        int count = 0;
//        while (count < a.length) {
//            sum += a[count][count];
//            count++;
//        }
        return sum;

    }

    static String firstSecond(Integer[] myArray) {
        Integer[] arr = myArray;
        Arrays.sort(arr, Collections.reverseOrder());
        int first = arr[0];
        Integer second = null;
        for (int i=0; i<arr.length; i++) {
            if (arr[i] != first) {
                second = arr[i];
                break;
            }
        }
        return first+" "+second;
//        int first = 0;
//        int second = 0;
//        for (Integer integer : myArray) {
//            if (first < integer) {
//                second = first;
//                first = integer;
//            } else if (second < integer) {
//                second = integer;
//            }
//        }
//        return first + " " + second;

    }

    static int missingNumber(int[] arr, int totalCount) {
        int sum1 = totalCount*(totalCount+1)/2;
        int sum2 = 0;
        for (int k : arr) {
            sum2 += k;
        }
        return sum1 - sum2;
//        int expectedSum = 0;
//        int actualSum = 0;
//        for(int i = 1; i <= totalCount; ++i) {
//            expectedSum += i;
//        }
//        for (int j : arr) {
//            actualSum += j;
//
//        }
//        System.out.println(expectedSum); // 5
//
//        return expectedSum - actualSum;
    }
}
