package com.company;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

class FibboMain {
  public static void main(String[] args) {
    RecursiveMain recursion = new RecursiveMain();
    int rec = fibonacci(10);
    System.out.println(rec);

  }

  public static int fibonacci(int n) {
    if (n < 0) {
      return -1;
    }
    if (n == 0 || n == 1) {
      return n;
    }
    int[] a = new int[n + 1];
    for (int i = 0; i <= n; i++) {
      if (i == 0) {
        a[i] = 0;
      } else if (i == 1) {
        a[i] = 1;
      } else {
        int s = a[i - 1] + a[i - 2];
        a[i] = s;
      }
    }
    return a[n];
//    if (n<0) {
//      return -1;
//    }
//    if (n==0 || n==1) {
//      return n;
//    }
//    return fibonacci(n-1) + fibonacci(n-2);
  }



}
