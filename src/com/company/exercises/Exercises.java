package com.company.exercises;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Exercises {
    public static void main(String[] args) {
//        int[] arr = {1,2,3,4,5,6,1,2};
//        System.out.println(Arrays.toString(removeDuplicates(arr)));
//        int[] myArray = {2,4,3,5,6,-2,4,7,8,5,9};
//        System.out.println(pairSum(myArray, 7));
        int[][] servers = {{1,0,0},{0,0,0},{0,0,1},{0,0,0}};
        System.out.println(updateServers(servers));
    }

    public static int updateServers (int[][] servers) {
        int noOfDays = 0;
        int[][] copyOfServers = new int[servers.length][servers[0].length];
        int noOfServersToUpdate = 0;
        for (int[] arr : servers) {
            for (int a : arr) {
                if (a == 0) {
                    noOfServersToUpdate++;
                }
            }
        }
        while (noOfServersToUpdate > 0) {
            for (int i = 0; i < servers.length; i++) {
                for (int j = 0; j < servers[0].length; j++) {
                    if (servers[i][j] == 1) {
                        if (copyOfServers[i][j] != 1) {
                            copyOfServers[i][j] = 1;
                        }
                        // check forward
                        if (j < servers[0].length-1 && servers[i][j+1] == 0) {
                            if (copyOfServers[i][j+1] != 1) {
                                copyOfServers[i][j + 1] = 1;
                                noOfServersToUpdate--;
                            }
                        }
                        // check backward
                        if (j > 0 && servers[i][j-1] == 0) {
                            if (copyOfServers[i][j-1] != 1) {
                                copyOfServers[i][j - 1] = 1;
                                noOfServersToUpdate--;
                            }
                        }
                        // check downward
                        if (i < servers.length-1 && servers[i+1][j] == 0) {
                            if (copyOfServers[i+1][j] != 1) {
                                copyOfServers[i + 1][j] = 1;
                                noOfServersToUpdate--;
                            }
                        }
                        // check upward
                        if (i > 0 && servers[i-1][j] == 0) {
                            if (copyOfServers[i-1][j] != 1) {
                                copyOfServers[i - 1][j] = 1;
                                noOfServersToUpdate--;
                            }
                        }
                    }
                }
            }
            System.out.println(Arrays.deepToString(copyOfServers));
            servers = copyOfServers;
            copyOfServers = new int[servers.length][servers[0].length];
            noOfDays++;
        }
        return noOfDays;
    }

    private static int removeDuplicates(int[] a, int n) {
        if (n == 0 || n == 1) {
            return n;
        }
        int j = 0;
        for (int i = 0; i < n - 1; i++) {
            if (a[i] != a[i + 1]) {
                a[j++] = a[i];
            }
        }
        a[j++] = a[n - 1];
        return j;
    }

    public static int[] removeDuplicates(int[] arr) {
//        int result = removeDuplicates(arr, arr.length);
//        int[] newArray = new int[result];
//        for (int i = 0; i < result; i++) {
//            newArray[i] = arr[i];
//        }
//        return newArray;

        int[] dupArr;
        int c = 0;
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = i+1; j < arr.length; j++) {
                if (arr[i] == arr[j]) {
                    count++;
                }
            }
        }
        System.out.println(count);
        dupArr = new int[arr.length - count];
        for (int j : arr) {
            int co = 0;
            for (int k : dupArr) {
                if (j == k) {
                    co++;
                }
            }
            if (co == 0) {
                dupArr[c] = j;
                c++;
            }
        }
        return dupArr;
    }

    public static String pairSum(int[] myArray, int sum) {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < myArray.length; i++) {
            for (int j = i+1; j < myArray.length; j++) {
                if (myArray[i] + myArray[j] == sum) {
                    str.append(myArray[i]).append(":").append(myArray[j]).append(" ");
                    break;
                }
            }
        }
        return str.toString();
    }
}
