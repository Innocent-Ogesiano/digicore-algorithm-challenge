package com.company;

import java.util.*;

public class SortCsv {
    public static void main(String[] args) {
        String csv_data = "Beth,Charles,Danielle,Adam,Eric\n17945,10091,10088,3987,10132\n2,12,13,48,11\n1,2,3,4,5";
        System.out.println(sortCsvColumns("Beth,Charles,Danielle,Adam,Eric\n17945,10091,10088,3987,10132"));
    }

    public static String sortCsvColumns( String csv_data ) {
        String[] strArr = csv_data.split("\n");
        StringBuilder a = new StringBuilder();
//        String a = "";
        if (!csv_data.contains("\n") && !csv_data.isEmpty()) {
            String[] arr = csv_data.split(",");
            Arrays.sort(arr);
//            List<String> lt = new ArrayList<>(Arrays.asList(arr));
//            Collections.sort(lt);
//            String[] s = new String[lt.size()];
//            s = lt.toArray(s);
            return String.join("," , arr);
        } else if (csv_data.isEmpty()) {
            return "";
        }
        else {
            int count = 0;
            Map<String, List<String>> listMap = new TreeMap<>();
            String[] keys = null;
            while (count < strArr.length) {
                if (count == 0) {
                    keys = strArr[count].split(",");
                } else {
                    String[] values = strArr[count].split(",");
                    for (int i = 0; i < values.length; i++) {
                        List<String> value;
                        if (listMap.containsKey(keys[i])) {
                            value = listMap.get(keys[i]);
                        } else {
                            value = new ArrayList<>();
                        }
                        value.add(values[i]);
                        listMap.put(keys[i], value);
                    }
                }
                count++;
            }
            List<List<String>> listList = new ArrayList<>();
            for (Map.Entry<String, List<String>> mapKeys : listMap.entrySet()) {
                a.append(mapKeys.getKey()).append(",");
                listList.add(mapKeys.getValue());
            }
            a.deleteCharAt(a.toString().lastIndexOf(",")).append("\n");
            int c = 0;
            int c2 = 0;
            while (c < listList.size() && c2 < listList.get(0).size()) {
                a.append(listList.get(c).get(c2)).append(",");
                c++;
                if (c == listList.size() && c2 < listList.get(0).size()) {
                    c = 0;
                    c2++;
                    a.deleteCharAt(a.toString().lastIndexOf(",")).append("\n");
                }
            }
            a.deleteCharAt(a.toString().lastIndexOf("\n"));
            return a.toString();
        }
//        else if (strArr.length == 2) {
//            String[] strArr1 = strArr[0].split(",");
//            String[] strArr2 = strArr[1].split(",");
//            TreeMap<String, List<String>> listMap = new TreeMap<>();
//            for (int i = 0; i < strArr1.length; i++) {
//                List<String> list = new ArrayList<>();
//                list.add(strArr2[i]);
//                listMap.put(strArr1[i],list);
//            }
//            StringBuilder st = new StringBuilder();
//            List<String> lt1 = new ArrayList<>();
//            List<String> lt2 = new ArrayList<>();
//            List<String> lt3 = new ArrayList<>();
//            for (Map.Entry<String,List<String>> map : listMap.entrySet() ) {
//                lt1.add(map.getKey());
//                lt2.add(map.getValue().get(0));
//            }
//            String[] s = new String[lt1.size()];
//            String[] s1 = new String[lt2.size()];
//            s = lt1.toArray(s);
//            s1 = lt2.toArray(s1);
//            a = String.join(",",s) + "\n" + String.join(",",s1);
//            return a;
//        } else {
//            String[] strArr1 = strArr[0].split(",");
//            String[] strArr2 = strArr[1].split(",");
//            String[] strArr3 = strArr[2].split(",");
//            TreeMap<String, List<String>> listMap = new TreeMap<>();
//            for (int i = 0; i < strArr1.length; i++) {
//                List<String> list = new ArrayList<>();
//                list.add(strArr2[i]);
//                list.add(strArr3[i]);
//                listMap.put(strArr1[i], list);
//            }
//            List<String> lt1 = new ArrayList<>();
//            List<String> lt2 = new ArrayList<>();
//            List<String> lt3 = new ArrayList<>();
//            for (Map.Entry<String, List<String>> map : listMap.entrySet()) {
//                lt1.add(map.getKey());
//                lt2.add(map.getValue().get(0));
//                lt3.add(map.getValue().get(1));
//            }
//            String[] s = new String[lt1.size()];
//            String[] s1 = new String[lt2.size()];
//            String[] s3 = new String[lt3.size()];
//            s = lt1.toArray(s);
//            s1 = lt2.toArray(s1);
//            s3 = lt3.toArray(s3);
//            a = String.join(",", s) + "\n" + String.join(",", s1) + "\n" + String.join(",", s3);
//            return a;
//        }
    }
}
