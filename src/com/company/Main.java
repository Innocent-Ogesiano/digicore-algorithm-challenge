package com.company;

import java.util.*;

public class Main {
    public static int numberOfSquares(int m, int n) {
        int sum = 0;
        while (m > 0 && n > 0) {
            sum += m * n;
            m = m - 1;
            n = n - 1;
        }
        return sum;
    }

    public static String mineSweeper(int n, int m) {
//        int[][] mines = new int[n][m];
        List<List<Integer>> mines = new ArrayList<>();

        return "";
    }

    static int recursiveMethod(int n) {
        if (n <= 1) {
            return 1;
        } else {
            return n * (recursiveMethod(n - 1));
        }
    }

    public static void main(String[] args) {
//        System.out.println(numberOfSquares(2,2));
        System.out.println(recursiveMethod(10));
    }
}
